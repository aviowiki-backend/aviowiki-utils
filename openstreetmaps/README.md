# OSM Stack

Note that a large number of disk space is required, at least 200GB recommended to hold the whole planet file and temporary space for filtering.

## Download planet file and filter for relevant relations (country borders)

```
wget http://planet.openstreetmap.org/pbf/planet-latest.osm.pbf
osmconvert planet-latest.osm.pbf -o=planet-latest.o5m
```

Alternatively, if you've already downloaded the file, you can use the 'osmupdate' utility to keep it up to date without redownloading everything.

Next, we filter the planet file for the relations we are interested in, currently thats the country borders (admin level 2)

```
osmfilter planet-latest.o5m --keep="type=boundary and boundary=administrative and admin_level=2 and ISO3166-1=" --drop-relations="admin_level!=2" -o=country_boundaries_2.o5m
osmconvert country_boundaries_2.o5m -o=country_boundaries_2.pbf
```

and the governing districts (states, provinces, it depends but basically one hierarchy below countries)

```
osmfilter planet-latest.o5m --keep="type=boundary and boundary=administrative and admin_level=4 and ISO3166-2=" --drop-relations="admin_level!=4" -o=country_boundaries_4.o5m
osmconvert country_boundaries_4.o5m -o=country_boundaries_4.pbf
```

## Import into PostGIS
Now that we have filtered the master data, we should have smaller files, around 200MB each that we can work with. First we need to import the data into postgresql.
Note we have a custom style file that is in this repository. We need to use it so that the ISO3166 tags are promoted to a database column instead of residing in the
flattened hstore, for better performance.

```
osm2pgsql --create --verbose --proj 4326 --hstore --prefix country_boundaries_2 --style ./country_boundaries.style --number-processes 8 --database aviowiki -H 192.168.99.100 -P 5438 -U postgres -W country_boundaries_2.pbf
osm2pgsql --create --verbose --proj 4326 --hstore --prefix country_boundaries_4 --style ./country_boundaries.style --number-processes 8 --database aviowiki -H 192.168.99.100 -P 5438 -U postgres -W country_boundaries_4.pbf
```

Thats it! The latest OSM data is now in aviowiki

# Other Geodata

Note that the following exports take a while, usually an hour or so. but they don't need to be done very often.

## Timezones

We get the latest shapefile from https://github.com/evansiroky/timezone-boundary-builder and then we simply import it into the geo database using QGIS export. Ensure to name the geometry column `way` instead of the default `geom`

## Oceans

We get the latest shapefile from https://osmdata.openstreetmap.de/data/water-polygons.html, using WGS84 projection, and then we simply import it into the geo database using QGIS export. Ensure to name the table `ocean-polygon` and the geometry column `way` instead of the default `geom`.

## Landmass

We get the latest shapefile from https://osmdata.openstreetmap.de/data/land-polygons.html, using WGS84 projection, and then we simply import it into the geo database using QGIS export. Ensure to name the table `land-polygon` and the geometry column `way` instead of the default `geom`.
