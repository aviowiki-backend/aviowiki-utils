#!/bin/sh

wget -O iso_3166-2.json https://salsa.debian.org/iso-codes-team/iso-codes/raw/master/data/iso_3166-2.json?inline=false
wget -O iso_3166-1.json https://salsa.debian.org/iso-codes-team/iso-codes/raw/master/data/iso_3166-1.json?inline=false

python3 json2csv.py iso_3166-2.json code,name,type code,name,administrative_type governing_district
python3 json2csv.py iso_3166-1.json alpha_2,alpha_3,name,numeric,official_name alpha2,alpha3,name,numeric3,official_name country
