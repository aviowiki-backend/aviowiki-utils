import csv
import json
import sys

json_data = open(sys.argv[1])
data = json.load(json_data)

standardName = sys.argv[1][4:-5]
csvfilename = sys.argv[1][:-4] + "csv"
sqlfilename = sys.argv[1][:-4] + "sql"
columns = sys.argv[2].split(",")
sqlcolumns = sys.argv[3].split(",")
with open(csvfilename, 'w') as csvfile, open(sqlfilename, 'w') as sqlfile:
    csvwriter = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
    csvwriter.writerow(columns)
    for row in data[standardName]:
        datarow = []
        for c in columns:
            datarow.insert(columns.index(c), row.get(c))
        csvwriter.writerow(datarow)
        filtered = list(filter(lambda t : t[1] is not None, zip(sqlcolumns, datarow)))
        sqlfile.write("INSERT INTO {} ({}) VALUES ({});\n".format(sys.argv[4], ",".join(map(lambda t : t[0], filtered)), ",".join(map(lambda t : t[1] if t[1].isnumeric() else "'" + t[1].replace("'", "''") + "'", filtered))))
