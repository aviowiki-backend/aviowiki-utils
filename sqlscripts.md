# delete recent opening hours contributions

```
delete from movement_availability_block mac where mac.parent_id in (select ma.id from movement_availability ma join movement_availability_contribution mac on (ma.id = mac.entity_id ) join contribution c on (c.id = mac.contribution_id ) where c."timestamp" >= '2020-04-03')
delete from movement_availability ma where ma.id in (select mac.entity_id from movement_availability_contribution mac join contribution c on mac.contribution_id = c.id where c."timestamp" >= '2020-04-03')
delete from movement_availability_block_contribution where id in(select macb.id from movement_availability_block_contribution macb join contribution c on macb.contribution_id = c.id where c."timestamp" >= '2020-04-03')
delete from movement_availability_contribution mac where id in (select mac.id from movement_availability_contribution mac join contribution c on c.id = mac.contribution_id where c."timestamp" >= '2020-04-03')

delete from arffavailability_block mac where mac.parent_id in (select ma.id from arffavailability ma join arffavailability_contribution mac on (ma.id = mac.entity_id ) join contribution c on (c.id = mac.contribution_id ) where c."timestamp" >= '2020-04-03')
delete from arffavailability ma where ma.id in (select mac.entity_id from arffavailability_contribution mac join contribution c on mac.contribution_id = c.id where c."timestamp" >= '2020-04-03')
delete from arffavailability_block_contribution where id in(select macb.id from arffavailability_block_contribution macb join contribution c on macb.contribution_id = c.id where c."timestamp" >= '2020-04-03')
delete from arffavailability_contribution mac where id in (select mac.id from arffavailability_contribution mac join contribution c on c.id = mac.contribution_id where c."timestamp" >= '2020-04-03')

delete from ciqavailability_block mac where mac.parent_id in (select ma.id from ciqavailability ma join ciqavailability_contribution mac on (ma.id = mac.entity_id ) join contribution c on (c.id = mac.contribution_id ) where c."timestamp" >= '2020-04-03')
delete from ciqavailability ma where ma.id in (select mac.entity_id from ciqavailability_contribution mac join contribution c on mac.contribution_id = c.id where c."timestamp" >= '2020-04-03')
delete from ciqavailability_block_contribution where id in(select macb.id from ciqavailability_block_contribution macb join contribution c on macb.contribution_id = c.id where c."timestamp" >= '2020-04-03')
delete from ciqavailability_contribution mac where id in (select mac.id from ciqavailability_contribution mac join contribution c on c.id = mac.contribution_id where c."timestamp" >= '2020-04-03')

delete from atcavailability_block mac where mac.parent_id in (select ma.id from atcavailability ma join atcavailability_contribution mac on (ma.id = mac.entity_id ) join contribution c on (c.id = mac.contribution_id ) where c."timestamp" >= '2020-04-03')
delete from atcavailability ma where ma.id in (select mac.entity_id from atcavailability_contribution mac join contribution c on mac.contribution_id = c.id where c."timestamp" >= '2020-04-03')
delete from atcavailability_block_contribution where id in(select macb.id from atcavailability_block_contribution macb join contribution c on macb.contribution_id = c.id where c."timestamp" >= '2020-04-03')
delete from atcavailability_contribution mac where id in (select mac.id from atcavailability_contribution mac join contribution c on c.id = mac.contribution_id where c."timestamp" >= '2020-04-03')
```
